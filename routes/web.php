<?php

use App\Http\Controllers\ANPRController;
use App\Http\Controllers\Safer;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [Safer::class, 'getIndex']);

Route::get('/home', [Safer::class, 'getIndex']);

Route::get ('/dashboard',[Safer::class, 'getIndex'] );
Route::get ('/login', [Safer::class, 'getLogin'] );
Route::get ('/register', [Safer::class, 'getRegister'] );
Route::get ('/forgot-password', [Safer::class, 'getForgotPassword']);


Route::get ('/api', [Safer::class, 'apiTest'] );
Route::get ('/image/{id}', [Safer::class, 'getImage'] );

Route::post ('/save-settings/{type}', [Safer::class, 'saveServerConfiguration'] );

// Face Regonition routes
Route::get ('/person-events/{personId}', [Safer::class, 'getPersonPassEvents'] );
Route::get ('/detail-page', [Safer::class, 'getFaceRegognitionDetailPage'] );
Route::get ('/face-rec', [Safer::class, 'getFaceRecognition'] );
Route::get ('/face-recognition', [Safer::class, 'getFaceRegognitionDetailPage'] );

// ANPR routes

Route::get ('/anpr', [ANPRController::class, 'getIndex'] );


// Route::get('/', [Safer::class, 'getLogin']);

// Route::get('/home', [Safer::class, 'getLogin']);

// Route::get ('/dashboard',[Safer::class, 'getLogin'] );
// Route::get ('/login', [Safer::class, 'getLogin'] );
// Route::get ('/register', [Safer::class, 'getLogin'] );
// Route::get ('/forgot-password', [Safer::class, 'getLogin']);


// Route::get ('/api', [Safer::class, 'getLogin'] );
// Route::get ('/image/{id}', [Safer::class, 'getLogin'] );

// Route::post ('/save-settings/{type}', [Safer::class, 'getLogin'] );

// // Face Regonition routes
// Route::get ('/person-events/{personId}', [Safer::class, 'getLogin'] );
// Route::get ('/detail-page', [Safer::class, 'getLogin'] );
// Route::get ('/face-rec', [Safer::class, 'getLogin'] );
// Route::get ('/face-recognition', [Safer::class, 'getLogin'] );

// // ANPR routes

// Route::get ('/anpr', [Safer::class, 'getLogin'] );