var searchByImage_placeHolderImage = new Image();
var isSearchingByFace = false;

function initSearchByImageUI() {
    searchByImage_placeHolderImage.onload = function() {
        let canvas = document.getElementById('searchByImageFace');
        if (isCanvasBlank(canvas)) {
            let context = canvas.getContext('2d');
            canvas.width = 100;
            context.drawImage(searchByImage_placeHolderImage, 0, 0, 100, 100);
        }
    };
    searchByImage_placeHolderImage.src = "/console/image/person-placeholder.jpg";
}

function isCanvasBlank(canvas) {
    const context = canvas.getContext('2d');
    if (canvas.width == 0 && canvas.height == 0)
        return true;
    const pixelBuffer = new Uint32Array(
        context.getImageData(0, 0, canvas.width, canvas.height).data.buffer
    );

    return !pixelBuffer.some(color => color !== 0);
}

function searchByImage() {
    $("#searchByImageDiv").css("display", "flex");
    if (!$("#searchByImageFilePath")[0].files[0])
        $('#searchByImageFilePath').trigger('click');
}

function selectSearchByImageFile() {
    var photo = document.getElementById('searchByImage');
    var fileToLoad = $("#searchByImageFilePath")[0].files[0];
    var fileReader = new FileReader();
    fileReader.onload = function(fileLoadedEvent) {
        var srcData = fileLoadedEvent.target.result;
        photo.src = srcData;
    }

    if (!!fileToLoad)
        fileReader.readAsDataURL(fileToLoad);
}

function showSearchByImageFile() {
    var photo = document.getElementById('searchByImage'); //selects the query named img
    var overlay = document.getElementById('searchByImageOverlay');

    overlay.width = photo.clientWidth;
    overlay.height = photo.clientHeight;

    postSearchPhoto(photo.src);
}

function postSearchPhoto(data) {
    $.ajax({
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        url: "/console/postPhoto?insert=false&update=false&merge=false&verbose=true&detect-mask=true",
        data: data,
        dataType: "json",
        success: function(result) {
            if (506 == result) {
                coviStatusCheck();
                return;
            }
            localStorage.setItem('faces', JSON.stringify(result.identifiedFaces));
            drawRectanglesForSearchSelection(result);
        },
        error: function(e) {
            console.log("ERROR: ", e);
            if (!!e.responseText)
                alert(i18n(e.responseText));
            if (e.responseJSON != null && e.responseJSON.message == "SessionTimeOutException")
                window.location.href = "/console/signin?url=" + encodeURIComponent(window.location.href);
        }
    });
}

function drawRectanglesForSearchSelection(result) {
    $('.faceInfo').remove();
    var canvas = document.getElementById('searchByImageOverlay');
    var rectangleDiv = document.getElementById('rectangleDiv');
    var context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.font = "bold 14px Georgia";
    var faces = result.identifiedFaces;
    for (j = 0; j < faces.length; j++) {
        var face = faces[j];
        var text = face.name;
        var width = face.relativeWidth * canvas.width;
        var height = face.relativeHeight * canvas.height;
        var x = face.offsetX * canvas.width;
        var y = face.offsetY * canvas.height;

        context.strokeStyle = "gray";
        context.fillStyle = "gray";
        text = localStorage.clickFaceToSelecttext;

        var e = document.createElement("div");
        e.className = "faceInfo";
        e.id = "fi_" + j;
        e.style.cssText = "left:" + x + "px;top:" + y + "px;width:" + width + "px;height:" + height + "px;pointer-events: none;";
        var html = "<div class=\"faceInfoIcon\" style=\"pointer-events: all;\"><img src=\"/console/image/info.png\"/><div class=\"faceQuality\">";
        var cpq = parseInt(face.attributes.centerPoseQuality * 100);
        var fcq = parseInt(face.attributes.contrastQuality * 100);
        var fsq = parseInt(face.attributes.sharpnessQuality * 100);
        var cpqStyle = ($("#cpq").val() || 59) <= cpq ? "green" : "red";
        var fcqStyle = ($("#fcq").val() || 45) <= fcq ? "green" : "red";
        var fsqStyle = ($("#fsq").val() || 45) <= fsq ? "green" : "red";
        html += "<span class=\"" + cpqStyle + "\">" + localStorage.centerposetext + ": " + cpq + "%</span>";
        html += "<span class=\"" + fcqStyle + "\">" + localStorage.contrasttext + ": " + fcq + "%</span>";
        html += "<span class=\"" + fsqStyle + "\">" + localStorage.sharpnesstext + ": " + fsq + "%</span>";
        html += "</div></div>";
        e.innerHTML = html;
        if (cpqStyle == "red" || fcqStyle == "red" || fsqStyle == "red")
            rectangleDiv.appendChild(e);

        context.lineWidth = 2;
        context.strokeRect(x, y, Math.min(canvas.width - x - 2, width), Math.min(canvas.height - y - 2, height));

        var textWidth = context.measureText(text).width;
        var textX = x + width / 2 - textWidth / 2;
        var textY = y + height + 20;
        if (textX < 4)
            textX = 4;
        if (textX + textWidth > canvas.width - 4)
            textX = canvas.width - 4 - textWidth;

        if (textY + 20 > canvas.height - 4)
            textY = y - 10;
        if (textY < 20)
            textY = 20;

        var fillStyle = context.fillStyle;
        context.fillStyle = "darkgray";
        context.fillRect(textX - 10, textY - 20, context.measureText(text).width + 20, 30);
        context.fillStyle = fillStyle;
        context.fillText(text, textX, textY);
    }

    if (faces.length == 0) {
        var text = localStorage.nofacedetectedtext;
        var textX = canvas.width / 2 - context.measureText(text).width / 2;
        var textY = canvas.height / 2;
        context.fillStyle = "darkgray";
        context.fillRect(textX - 10, textY - 20, context.measureText(text).width + 20, 30);
        context.fillStyle = "#FFDF00";
        context.fillText(text, textX, textY);
    }
}

function selectPeopleForSearch(e) {
    let canvas = document.getElementById('searchByImageFace');
    if ($("#searchByImageFilePath")[0].files[0] == undefined || isCanvasBlank(canvas) || $("#searchByImageFilePath")[0].files[0].name.toLowerCase().endsWith('.heic'))
        return;
    let faces = JSON.parse(localStorage.getItem('faces'));
    let overlay = document.getElementById('searchByImageOverlay');
    let rect = overlay.getBoundingClientRect();
    let x = e.clientX - rect.left;
    let y = e.clientY - rect.top;
    for (j = 0; j < faces.length; j++) {
        let face = faces[j];
        let a = x / overlay.width;
        let b = y / overlay.height;
        if (a > face.offsetX && a < face.offsetX + face.relativeWidth &&
            b > face.offsetY && b < face.offsetY + face.relativeHeight) {
            let context = canvas.getContext('2d');
            let image = document.getElementById('searchByImage');
            let w = face.relativeWidth * image.naturalWidth;
            let h = face.relativeHeight * image.naturalHeight;
            let sx = Math.max(0, (face.offsetX - face.relativeWidth * 0.25) * image.naturalWidth);
            let sy = Math.max(0, (face.offsetY - face.relativeHeight * 0.25) * image.naturalHeight);
            let ex = Math.min(1, (face.offsetX + face.relativeWidth * 1.25)) * image.naturalWidth;
            let ey = Math.min(1, (face.offsetY + face.relativeHeight * 1.25)) * image.naturalHeight;
            let sWidth = ex - sx;
            let sHeight = ey - sy;
            canvas.width = sWidth;
            canvas.height = sHeight;
            context.clearRect(0, 0, sWidth, sHeight);
            context.drawImage(image, sx, sy, sWidth, sHeight, 0, 0, sWidth, sHeight);
            canvas.style.zoom = 100 / sHeight;
            window.removeEventListener('scroll', scrollFunction);
            document.getElementById("searchByImageDiv").style.display = "none";
            $("#clearSearchImageBtn").show();
            $("#selectImageBtn").text(localStorage.replaceSearchImagetext);
            postFaceSearch(canvas.toDataURL('image/jpeg'));
        }
    }
}

function clearSearchByImage() {
    let url = new URL(location.href);
    url.searchParams.delete('sid_P');
    url.searchParams.set('action', 'refresh');
    sessionStorage.removeItem("EVENT_FACE_SEARCH_ID");
    sessionStorage.removeItem("EVENT_FACE_SEARCH_DATA");
    window.location = url;
}

function shouldShow(face) {
    //filter by name, personType, idClass, and homeLocation
    let name = $('#name').val();
    let personType = $('#personType').val();
    let idClass = $('#idClass').val();
    let homeLocation = $('#home').val();

    if (!!name && !!face.name && !face.name.includes(name))
        return false;
    if (!!name && !face.name)
        return false;
    if (personType == "$present" && !face.personType)
        return false;
    if (personType == "$absent" && !!face.personType)
        return false;
    if (!!personType && personType != "$present" && personType != "$absent" && face.personType != personType)
        return false;

    if (!!idClass && face.idClass != idClass)
        return false;

    if (homeLocation == "$present" && !face.homeLocation)
        return false;
    if (homeLocation == "$absent" && !!face.homeLocation)
        return false;
    if (!!homeLocation && homeLocation != "$present" && homeLocation != "$absent" && face.homeLocation != homeLocation)
        return false;

    return true;
}

function showSimilar(p) {
    if (!shouldShow(p)) return;
    var unknownPerson_src = "/console/image/person-placeholder.jpg";

    var e = document.createElement("div");
    e.id = "p_" + p.personId;
    e.className = "listItem100 marginBottom5";
    var idClassStyle = "gray";
    var similarScoreBGColor = "#267BB6";
    if ('threat'.equalIgnoreCase(p.idClass)) {
        idClassStyle = "red";
        similarScoreBGColor = "red";
    } else if ('concern'.equalIgnoreCase(p.idClass)) {
        idClassStyle = "orange";
        similarScoreBGColor = "orange";
    }

    var html = "<table><tr>";
    html += "<td class=\"cbtd\"><input type=\"checkbox\" class=\"check\" tag=\"root\" id=\"cb_" + p.personId + "\" onclick=\"event.cancelBubble=true;checkboxClicked('" + p.personId + "');\"/></div>";
    html += "<td class=\"portrait\"><div onmouseover=\"mouseOverFace(this, true)\" onmouseout=\"mouseOutFace(this)\" onclick=\"event.cancelBubble=true;clickCheckbox('" + p.personId + "');checkboxClicked('" + p.personId + "');\" class=\"portrait " + idClassStyle + "\">";
    html += "<img class=\"portraitEvent\" id=\"" + p.personId + "\" src=\"" + unknownPerson_src + "\"/>";

    html += "<div class=\"tooltipDiv\" ><img class=\"searchIcon\" src=\"/console/image/search-face.png\"/><div class=\"tooltiptext1\"><div class=\"menuItem\" onclick=\"event.cancelBubble=true;searchFaceImage(this)\">" + localStorage.searchByFacetext + "</div><div class=\"menuItem\" onclick=\"event.cancelBubble=true;searchFaceImageInEvents(this)\">" + localStorage.searchByFaceInEventstext + "</div></div></div></div>";

    //hasMergedPeople returns only in getRootPeople, not working in current version.
    if (p.hasMergedPeople) html += "<img class=\"expand\" src=\"/console/image/expand.svg\" onclick=\"event.cancelBubble=true;loadMergedPeople('" + p.personId + "');rotate(this);\"/>";
    html += "</div></td>"
    html += "<td class=\"fill\" onclick=\"editPeople('" + p.personId + "')\">";

    let similarityScore = (Math.floor(p.similarityScore * 100));
    if (similarityScore > 100) similarityScore = 100;
    html += "<span class=\"similarityScore\" style=\"background-color:" + similarScoreBGColor + "\">" + similarityScore + "%</span>";
    if (!!p.name) html += "<span class=\"inline marginRight10\">" + p.name + "</span>";
    if (!!p.personType) html += "<span class=\"block\">" + p.personType + "</span>";
    if (!!p.personId) html += "<span class=\"block\">" + p.personId + "</span>";
    if (!!p.externalId) html += "<span class=\"block\">" + p.externalId + "</span>";
    if ('male'.equalIgnoreCase(p.gender))
        html += "<span class=\"inline\">" + localStorage.maletext + "</span>";
    else if ('female'.equalIgnoreCase(p.gender))
        html += "<span class=\"inline\">" + localStorage.femaletext + "</span>";
    else if (!!p.gender)
        html += "<span class=\"inline\">" + p.gender + "</span>";

    if (!!p.age) html += "<span class=\"inline\">, " + localStorage.agetext + " " + ((p.age) ? Math.round(p.age) : "") + "</span>";
    if (!!p.allTags) html += "<span class=\"block\">" + p.allTags + "</span>";
    html += "</td>";
    html += "</tr></table>";
    html += "<div class=\"personDiv\" id=\"l_" + p.personId + "\"></div>";
    e.innerHTML = html;
    document.getElementById("list").appendChild(e);
    processImages();
}

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function searchImageDataUrl(dataUrl) {
    var image = new Image();
    image.onload = function() {
        let canvas = document.getElementById('searchByImageFace');
        let context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);
        canvas.width = image.naturalWidth;
        canvas.height = image.naturalHeight;
        canvas.style.zoom = 100 / canvas.height;
        context.drawImage(image, 0, 0, canvas.width, canvas.height);
        window.removeEventListener('scroll', scrollFunction);
        document.getElementById("searchByImageDiv").style.display = "none";
        $("#clearSearchImageBtn").show();
        $("#selectImageBtn").text(localStorage.replaceSearchImagetext);
        postFaceSearch(dataUrl);
    };
    image.src = dataUrl;
}