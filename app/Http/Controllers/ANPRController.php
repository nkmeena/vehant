<?php
namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Arr;

class ANPRController extends  Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getIndex(Request $request)
    {
        $inputs = $request->all();
        if(empty($request->input('range'))){
            $inputs["range"] = "7";
        }else{
            $inputs["range"] = $request->input('range');

        }
        $config_data = $this->getServerConfiguration();
        $response = [];
        if(!empty($inputs["lpnum"])){
            $response =  $this->anprAPIRequest("GET" , "getVehicleRoute" , ["get"=>$inputs , "server_details"=> $config_data]);
        }else{
            $response =  $this->anprAPIRequest("GET" , "getANPRDataList" , ["get"=>$inputs , "server_details"=> $config_data]);
        }
        $transaction_list =  data_get( $response , "Transactions.TransactionList" , []);
        $sidebar = $this->getSidebar();
        $config_data = $this->getServerConfiguration();

        $result = $this->getGraphData( $transaction_list ,  $inputs["range"] );

        $datetime_ratio = data_get(  $result , "datetime_ratio" , []);

        return view('real-networks.anpr.index' , ['events' => [] , "datetime_ratio"  =>$datetime_ratio , 
        "transaction_list"=>$transaction_list , "config_data"=> $config_data , 'sidebar'=>$sidebar ]);
    }

    public function getGraphData($transaction_list , $range){
        $result = [];
        // will change this hardcode
        $dates = $this->getDateRange( $range);

        foreach($transaction_list as $transaction){
            $epoch = ceil( data_get($transaction ,'TimeStamp' , 0));
            if(!empty($epoch)){
                $dt = new DateTime("@$epoch");  // convert UNIX timestamp to PHP DateTime
                $date  =  $dt->format('Y-m-d'); // output = 2017-01-01
                if(Arr::has($dates , $date)){
                    $dates[$date]++;
                }
            }
        }
        $result["datetime_ratio"] = $dates;
        return $result;
    }
}